import FYBaseUI from "../Tools/FYBaseUI";
import FYMessenger from "../Tools/FYMessenger";
import FYUIMgr from "../Tools/FYUIMgr";
import { FYE } from "../Tools/FYE";

const { ccclass, property } = cc._decorator;
/** 菜单界面 */
@ccclass
export default class UIMenu extends FYBaseUI {

    /** 数组按钮 */
    @property(cc.Node)
    public btnArray: cc.Node = null;

    addListener() {
        super.addListener();

        this.btnArray.on(cc.Node.EventType.TOUCH_END, this.onClickBtnArray, this);
    }

    removeListener() {
        super.removeListener();

        this.btnArray.off(cc.Node.EventType.TOUCH_END, this.onClickBtnArray, this);
    }

    // -------------------------------- 回调函数 --------------------------------- //

    onShow(msgType, param) {

    }

    onClickBtnArray() {
        FYUIMgr.Instance.show(FYE.UIName.UIArray);
    }

    // -------------------------------- 生命周期 --------------------------------- //
}
