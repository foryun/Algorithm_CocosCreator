import FYBaseUI from "../Tools/FYBaseUI";
import FYUIMgr from "../Tools/FYUIMgr";
import { FYE } from "../Tools/FYE";

const { ccclass, property } = cc._decorator;
/** 数组界面 */
@ccclass
export default class UIArray extends FYBaseUI {
    /** 关闭按钮 */
    @property(cc.Node)
    public btnClose: cc.Node = null;

    addListener() {
        super.addListener();

        this.btnClose.on(cc.Node.EventType.TOUCH_END, this.onClickBtnClose, this);
    }

    removeListener() {
        super.removeListener();

        this.btnClose.off(cc.Node.EventType.TOUCH_END, this.onClickBtnClose, this);
    }

    // -------------------------------- 回调函数 --------------------------------- //

    onShow(msgType, param) {

    }

    onClickBtnClose() {
        FYUIMgr.Instance.close(FYE.UIName.UIArray);
    }

    // -------------------------------- 生命周期 --------------------------------- //
}
