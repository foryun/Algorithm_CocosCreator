
const { ccclass, property } = cc._decorator;

@ccclass
export default class Demo extends cc.Component {
    /** 清除全部 */
    @property(cc.Node)
    public btnClearAll: cc.Node = null;

    start() {
        cc.log(cc.loader);
        this.btnClearAll.on(cc.Node.EventType.TOUCH_END, this.onClickBtnClearAll, this);
    }

    onClickBtnClearAll() {
        
        let sprite = this.btnClearAll.getComponent(cc.Sprite);
        // this.btnClearAll.destroy();
        cc.loader.releaseRes(sprite.spriteFrame["_textureFilename"])
        
        cc.log(cc.loader);
        this.schedule(function () {
            cc.log(cc.loader);
        }, 2, 1, 1);
    }
}
