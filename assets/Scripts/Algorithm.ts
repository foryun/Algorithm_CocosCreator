
export default class Algorithm {
    /**
     * 冒泡排序
     * 选择排序
     * 插入排序
     * 希尔排序
     * 快速排序
     * 归并排序
     * 堆排序
     * 基数排序
     */

    /**
     * 交换
     * @param arrayNum 待排序数组
     * @param index1 下标1
     * @param index2 下标2
     */
    public static swap(arrayNum, index1, index2) {
        let tmp = arrayNum[index1];
        arrayNum[index1] = arrayNum[index2];
        arrayNum[index2] = tmp;
    }

    /**
     * 冒泡排序
     * @param arrayNum 待排序数组
     */
    public static sortBubble(arrayNum: any[]) {
        for (let i = 0; i < arrayNum.length; i++) {
            for (let j = arrayNum.length - 1; j > 0; j--) {
                if (arrayNum[j] < arrayNum[j - 1]) {
                    Algorithm.swap(arrayNum, j, j - 1);
                }
            }
        }
    }

    /**
     * 选择排序
     * @param arrayNum 待排序数组
     */
    public static sortSelection(arrayNum: any[]) {
        for (let i = 0; i < arrayNum.length - 1; i++) {
            let index = i;
            // 找到最小的数
            for (let j = i + 1; j < arrayNum.length; j++) {
                if (arrayNum[j] < arrayNum[index]) {
                    index = j;
                }
            }
            Algorithm.swap(arrayNum, i, index);
        }
    }

    /**
     * 插入排序
     * @param arrayNum 待排序数组
     */
    public static sortInsertion(arrayNum: any[]) {
        for (let i = 1; i < arrayNum.length; i++) {
            for (let j = i - 1; j >= 0; j--) {
                if (arrayNum[i] < arrayNum[j]) {
                    Algorithm.swap(arrayNum, i, j);
                }
            }
        }
    }

    /**
     * 希尔排序
     * @param arrayNum 待排序数组
     */
    public static sortShell(arrayNum: any[]) {

    }

    public

    public static sortQuick() {

    }
}
